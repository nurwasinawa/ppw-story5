from django import forms
from .models import Schedule
from django.forms import ModelForm

class ScheduleForm(ModelForm):
    class Meta:
        model = Schedule
        fields = ['title','lecturer','location','category','description']
        labels = {'title':'Courses','lecturer':'Lecturer','location':'Class','category':'Period','description':'Description'}
        widgets = {
            'title':forms.TextInput(attrs={'class':'form-control','type':'text'}),
            'lecturer':forms.TextInput(attrs={'class':'form-control','type':'text'}),
            'location':forms.TextInput(attrs={'class':'form-control','type':'text'}),
            'category':forms.Select(attrs={'class':'form-control'}),
            'description':forms.Textarea(attrs={'class':'form-control','type':'textarea','row':'10'}),
            }
