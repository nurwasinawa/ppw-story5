from django.db import models
import datetime

# Create your models here.
CATEGORY=[('Gasal 2018/2019' , 'Gasal 2018/2019'),
            ('Genap 2018/2019' , 'Genap 2018/2019'),
			('Gasal 2019/2020' , 'Gasal 2019/2020'),
            ('Genap 2019/2020' , 'Genap 2019/2020')]

class Schedule(models.Model):
    title = models.CharField(max_length=100)
    lecturer = models.CharField(max_length=100)
    location = models.CharField(max_length=100)
    category = models.CharField(max_length=100, choices=CATEGORY)
    description = models.CharField(null=True, max_length=100)

